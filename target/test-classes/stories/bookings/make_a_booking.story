Meta:

Narrative:
As a user
I want to perform a booking
So that I can to travel

Scenario: Failed booking
Meta:
@id testFailedBooking
Given I make a booking from 'BOG' to 'MAD' on '30/09/2017' and '' for '2' adult(s) and '1' child(s) loged with 'prodsistemas@yahoo.es' user and 'Micuentaderyanair12' password
And I select the flight of my preference
When I fill the passenger details:
| title | firstName | lastName |
| Mr    | Fernando  | Alonso  |
| Mrs   | Amaia     | Montero |
|       | Julian    | Alonso  |
And I pay for booking with the details:
| phoneNumberCountry | phoneNumber | cardNumber       | cardType   | expiryDate | securityCode | cardHolderName  | billingAddressAddressLine1 | billingAddressCity |
| Spain              | 123456789   | 5555555555555557 | MasterCard | 10/2018      | 123          | Fernando Alonso | 5th avenue                 | Madrid             |
Then I should get payment declined message