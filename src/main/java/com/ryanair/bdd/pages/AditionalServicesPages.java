package com.ryanair.bdd.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.WebDriver;

import java.util.List;

public class AditionalServicesPages extends PageObject{
    @FindBy(xpath = "//div[@class='button-next']/button")
    WebElementFacade btnCheckOutFooter;

    public AditionalServicesPages(WebDriver driver) {
        super(driver);
    }

    public void checkOut(){
        if(btnCheckOutFooter.isCurrentlyVisible()) {
            btnCheckOutFooter.waitUntilVisible()
                    .waitUntilClickable()
                    .click();
            btnCheckOutFooter.waitUntilNotVisible();
        }
    }
}
