package com.ryanair.bdd.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.jbehave.core.model.ExamplesTable;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class PaymentPages extends PageObject{
    private int position = 0;
    private String title = "//select[contains(@name,'title')]";
    private String firstName = "//input[contains(@name,'firstName')]";
    private String lastName = "//input[contains(@name,'lastName')]";
    @FindBy(xpath = "//select[@name='phoneNumberCountry']")
    WebElementFacade cmbPhoneNumberCountry;
    @FindBy(xpath = "//input[@name='phoneNumber']")
    WebElementFacade txtPhoneNumber;
    @FindBy(xpath = "//input[@name='billingAddressCity']")
    WebElementFacade txtBillingAddressCity;
    @FindBy(xpath = "//input[@name='billingAddressAddressLine1']")
    WebElementFacade txtBillingAddressAddressLine1;
    @FindBy(xpath = "//input[@name='cardNumber']")
    WebElementFacade txtCardNumber;
    @FindBy(xpath = "//select[@name='cardType']")
    WebElementFacade cmbCardType;
    @FindBy(xpath = "//select[@name='expiryMonth']")
    WebElementFacade cmbExpiryMonth;
    @FindBy(xpath = "//select[@name='expiryYear']")
    WebElementFacade cmbExpiryYear;
    @FindBy(xpath = "//input[@name='securityCode']")
    WebElementFacade txtSecurityCode;
    @FindBy(xpath = "//input[@name='cardHolderName']")
    WebElementFacade txtCardHolderName;
    @FindBy(xpath = "//div[@class='cta']//input")
    WebElementFacade chkAcceptTerms;
    @FindBy(xpath = "//div[@class='cta']//button")
    WebElementFacade btnPayNow;

    public int getPosition() {
        return position;
    }

    public void setPosition() {
        this.position = position + 1;
    }

    public PaymentPages(WebDriver driver) {
        super(driver);
    }

    public void fillPassengerDetails(ExamplesTable passengerDetails) {
        waitFor("//div[@passengers-form]//passenger-input-group//select[contains(@name,'title')]");
        passengerDetails.getRows().stream()
                .forEach( group -> {
                    String passengerInputGroup = "//div[@passengers-form]//passenger-input-group[" + (getPosition() + 1) +"]";
                    if(!"".equals(passengerDetails.getRow(getPosition()).get("title")))
                        $(passengerInputGroup + title).sendKeys(passengerDetails.getRow(getPosition()).get("title"));
                    if(!"".equals(passengerDetails.getRow(getPosition()).get("firstName")))
                        $(passengerInputGroup + firstName).sendKeys(passengerDetails.getRow(getPosition()).get("firstName"));
                    if(!"".equals(passengerDetails.getRow(getPosition()).get("lastName")))
                        $(passengerInputGroup + lastName).sendKeys(passengerDetails.getRow(getPosition()).get("lastName"));
                    setPosition();
                }
        );
    }

    public void fillPaymentData(ExamplesTable paymentData) {
        paymentData.getHeaders().stream()
                .forEach(
                        header -> {
                            if(!"".equals(paymentData.getRow(0).get(header)))
                                fillInput(header, paymentData.getRow(0).get(header));
                        }
                );
    }

    private void fillInput(String header, String data) {
        switch (header){
            case "phoneNumberCountry":
                cmbPhoneNumberCountry.selectByVisibleText(data);
                break;
            case "phoneNumber":
                txtPhoneNumber.type(data);
                break;
            case "cardNumber":
                txtCardNumber.type(data);
                break;
            case "cardType":
                cmbCardType.selectByVisibleText(data);
                break;
            case "expiryDate":
                String[] splitExpiryDate = data.split("[/-]");
                cmbExpiryMonth.selectByVisibleText(splitExpiryDate[0]);
                cmbExpiryYear.selectByVisibleText(splitExpiryDate[1]);
                break;
            case "securityCode":
                txtSecurityCode.type(data);
                break;
            case "cardHolderName":
                txtCardHolderName.type(data);
                break;
            case "billingAddressAddressLine1":
                txtBillingAddressAddressLine1.type(data);
                break;
            case "billingAddressCity":
                txtBillingAddressCity.type(data + Keys.TAB + Keys.TAB + Keys.TAB + Keys.SPACE);
                break;
            default:
                break;
        }
    }

    public void payNow() {
        btnPayNow.click();
    }

    public boolean declinedMessege() {
        return $("xpath_to_declined_messege").isCurrentlyVisible();
    }
}
