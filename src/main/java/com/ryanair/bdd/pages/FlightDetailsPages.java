package com.ryanair.bdd.pages;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.WebDriver;

import java.util.List;

public class FlightDetailsPages extends PageObject{
    String priceType = "//div[@class='flights-table-fares']/div//div//span[contains(.,'TYPE')]/../..//button";
    @FindBy(xpath = "//flights-table-ops-price//div[contains(@class,'fare-select')]")
    List<WebElementFacade> flightsOptions;
    @FindBy(xpath = "//div[@class='flight-selector__listing-footer']/button")
    WebElementFacade btnContinueFooter;

    public FlightDetailsPages(WebDriver driver) {
        super(driver);
    }

    public void selectFlight() {
        flightsOptions.stream()
                .findFirst()
                .get()
                .click();
    }

    public void selectPriceType(String type){
        waitABit(2000);
        String typeOfPrice;
        typeOfPrice = priceType;
        typeOfPrice = typeOfPrice.replace("TYPE", type);
        if($(typeOfPrice).isVisible()) {
            $(typeOfPrice).click();
        }
    }

    public void continueWithBooking() {
        btnContinueFooter.waitUntilVisible()
                .waitUntilClickable()
                .click();
        btnContinueFooter.waitUntilNotVisible();
    }
}