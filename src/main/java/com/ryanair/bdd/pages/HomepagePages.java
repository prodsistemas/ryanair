package com.ryanair.bdd.pages;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

@DefaultUrl("https://www.ryanair.com/ie/en/")
public class HomepagePages extends PageObject{
    int position;
    String passengerDetails = "//popup-content//div[@passenger-detail]//div[contains(.,'PASSENGER_TYPE')]//input";
    @FindBy(xpath = "//div[@id='menu-container']//a[contains(.,'Log in')]")
    WebElementFacade lnkLogIn;
    @FindBy(xpath = "//div[@dialog]//input[@type='email']")
    WebElementFacade txtUser;
    @FindBy(xpath = "//div[@dialog]//input[@type='password']")
    WebElementFacade txtPassword;
    @FindBy(xpath = "//div[@dialog]//button[@type='submit']")
    WebElementFacade btnLoginDialog;
    @FindBy(xpath = "//label[.='From:']/following-sibling::div//input")
    WebElementFacade txtFrom;
    @FindBy(xpath = "//label[.='To:']/following-sibling::div//input")
    WebElementFacade txtTo;
    @FindBy(xpath = "//label[.='Fly out:']/following-sibling::div//input")
    List<WebElementFacade> txtFlyOut;
    @FindBy(xpath = "//label[.='Fly back:']/following-sibling::div//input")
    List<WebElementFacade> txtFlyBack;
    @FindBy(xpath = "//div[@core-rich-input and @label='Passengers:']")
    WebElementFacade cmbPassengers;
    @FindBy(xpath = "//span[contains(.,'go!')]/..")
    WebElementFacade btnGo;
    @FindBy(xpath = "//div[@flight-search-type]")
    WebElementFacade optFlightSearchType;
    @FindBy(xpath = "//div[@class='promo-popup-close']")
    WebElementFacade popupPromoClose;
    @FindBy(xpath = "//cookie-pop-up//core-icon")
    WebElementFacade closeCookiePopup;

    public int getPosition() {
        return position;
    }

    private void setPosition() {
        this.position = position + 1;
    }

    public HomepagePages(WebDriver driver) {
        super(driver);
    }

    public void fillFrom(String from) {
        txtFrom.typeAndTab(from);
    }

    public void fillTo(String to) {
        txtTo.typeAndTab(to);
    }

    public void fillFlyOut(String flyOut) {
        position = 0;
        String[] txtDate = flyOut.split("[/-]");
        txtFlyOut.forEach(input -> {
            input.type(txtDate[getPosition()]);
            waitABit(500);
            setPosition();
        });
    }

    public void fillFlyBack(String flyBack) {
        if(!"".equalsIgnoreCase(flyBack)){
            position = 0;
            String[] txtDate = flyBack.split("[/-]");
            txtFlyBack.forEach(input -> {
                input.type(txtDate[getPosition()]);
                waitABit(500);
                setPosition();
            });
        }
    }

    public void fillPassengers(String passengerType, String passengerQuantity) {
        String passenger = passengerDetails;
        passenger = passenger.replace("PASSENGER_TYPE", passengerType);
        $(passenger).typeAndTab(passengerQuantity);
    }

    public void go() {
        btnGo.click();
    }

    public void flightSearchType(String type) {
        optFlightSearchType.findElement(By.xpath("//span[contains(.,'"+ type +"')]/../../input")).click();
    }

    public void openPassengersPopup() {
        cmbPassengers.click();
    }

    public void closePopup(){
        if(popupPromoClose.isCurrentlyVisible())
            popupPromoClose.click();
    }

    public void closeCookiePopup(){
        closeCookiePopup.waitUntilClickable()
                .click();
    }

    public void logIn(String user, String password) {
        lnkLogIn.waitUntilClickable()
                .click();
        txtUser.waitUntilClickable()
                .typeAndTab(user);
        txtPassword.waitUntilClickable()
                .typeAndTab(password);
        btnLoginDialog.waitUntilClickable()
                .click();
        txtUser.waitUntilNotVisible();
    }
}
