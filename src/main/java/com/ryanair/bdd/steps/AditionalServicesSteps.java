package com.ryanair.bdd.steps;

import com.ryanair.bdd.pages.AditionalServicesPages;
import com.ryanair.bdd.pages.FlightDetailsPages;
import net.thucydides.core.steps.ScenarioSteps;

/**
 * Created by Pedro Rodriguez on 24/08/17.
 */
public class AditionalServicesSteps extends ScenarioSteps{
    AditionalServicesPages aditionalServicesPages;

    public void checkOut() {
        aditionalServicesPages.checkOut();
    }
}
