package com.ryanair.bdd.steps;

import com.ryanair.bdd.pages.HomepagePages;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

import static org.bouncycastle.cms.RecipientId.password;

public class HomepageSteps extends ScenarioSteps {
    HomepagePages homepagePages;

    @Step
    public void fillFlightData(String from, String to, String flyOut, String flyBack, String forAdults, String forChild) {
        homepagePages.fillFrom(from);
        homepagePages.fillTo(to);
        homepagePages.fillFlyOut(flyOut);
        homepagePages.fillFlyBack(flyBack);
        homepagePages.openPassengersPopup();
        homepagePages.fillPassengers("Adults", forAdults);
        homepagePages.fillPassengers("Children", forChild);
        homepagePages.go();
        homepagePages.closePopup();
    }

    @Step
    public void flightSearchType(String type) {
        homepagePages.flightSearchType(type);
    }

    @Step
    public void closeCookiePopup() {
        homepagePages.closeCookiePopup();
    }

    @Step
    public void logIn(String user, String password) {
        homepagePages.open();
        homepagePages.logIn(user, password);
    }
}
