package com.ryanair.bdd.steps;

import com.ryanair.bdd.pages.FlightDetailsPages;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

/**
 * Created by Pedro Rodriguez on 23/08/17.
 */
public class FlightDetailsSteps extends ScenarioSteps{
    FlightDetailsPages flightDetailsPages;

    @Step
    public void selectFlight() {
        selectFlightOption();
        selectPriceType("Standard fare");
        flightDetailsPages.continueWithBooking();
    }

    @Step
    public void selectFlightOption() {
        flightDetailsPages.selectFlight();
    }

    @Step
    public void selectPriceType(String priceOption) {
        flightDetailsPages.selectPriceType(priceOption);
    }
}
