package com.ryanair.bdd.steps;

import com.ryanair.bdd.pages.AditionalServicesPages;
import com.ryanair.bdd.pages.PaymentPages;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import org.hamcrest.MatcherAssert;
import org.jbehave.core.model.ExamplesTable;

/**
 * Created by Pedro Rodriguez on 24/08/17.
 */
public class PaymentSteps extends ScenarioSteps{
    PaymentPages paymentPages;

    @Step
    public void fillPaymentData(ExamplesTable paymentData) {
        paymentPages.fillPaymentData(paymentData);
        paymentPages.payNow();
    }

    @Step
    public void fillPassengerDetails(ExamplesTable passengerDetails) {
        paymentPages.fillPassengerDetails(passengerDetails);
    }

    @Step
    public void verifyDeclinedMessege() {
        MatcherAssert.assertThat("Declined messege is not present!", !paymentPages.declinedMessege());
    }
}
