package com.ryanair.bdd.definitions;

import com.ryanair.bdd.steps.FlightDetailsSteps;
import com.ryanair.bdd.steps.HomepageSteps;
import com.ryanair.bdd.steps.PaymentSteps;
import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.model.ExamplesTable;

public class MakeABookingDefinitions {
    @Steps
    HomepageSteps homepageStepSteps;
    @Steps
    FlightDetailsSteps flightDetailsSteps;
    @Steps
    PaymentSteps paymentSteps;

    @Given("I make a booking from '$from' to '$to' on '$flyOut' and '$flyBack' for '$forAdults' adult(s) and '$forChild' child(s) loged with '$user' user and '$password' password")
    public void makeABookingFromTo(String user, String password, String from, String to, String flyOut, String flyBack, String forAdults, String forChild){
        homepageStepSteps.logIn(user, password);
        homepageStepSteps.closeCookiePopup();
        homepageStepSteps.flightSearchType("One way");
        homepageStepSteps.fillFlightData(from, to, flyOut, flyBack, forAdults, forChild);
    }

    @Given("I select the flight of my preference")
    public void selectFlight(){
        flightDetailsSteps.selectFlight();
    }

    @When("I fill the passenger details: $passengerDetails")
    public void fillPassengerDetails(ExamplesTable passengerDetails){
        paymentSteps.fillPassengerDetails(passengerDetails);
    }

    @When("I pay for booking with the details: $details")
    public void payForBooking(ExamplesTable details){
        paymentSteps.fillPaymentData(details);
    }

    @Then("I should get payment declined message")
    public void getPaymentDeclined(){
        paymentSteps.verifyDeclinedMessege();
    }
}
